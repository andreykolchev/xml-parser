package com.parser;

import com.parser.config.StreamProcessor;
import com.parser.dao.TransactionDao;
import com.parser.model.Client;
import com.parser.model.TransactionEntity;
import com.parser.parser.TransactionParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;


public class ApplicationTest {

    TransactionEntity entity;
    TransactionDao transactionDao;

    @Before
    public void setUp() {
        transactionDao = Mockito.mock(TransactionDao.class);
        entity = new TransactionEntity();
        Client client = new Client();
        client.setFirstName("Ivan");
        client.setLastName("Ivanoff");
        client.setMiddleName("Ivanoff");
        client.setInn(1234567890);
        entity.setPlace("A PLACE 1");
        entity.setAmount(10.01F);
        entity.setCurrency("UAH");
        entity.setCard("123456****1234");
        entity.setClient(client);
        Mockito.when(transactionDao.save(Mockito.isA(TransactionEntity.class))).thenReturn(entity);
    }

    @Test
    public void main() throws XMLStreamException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream("test.xml");
        TransactionParser transactionParser = new TransactionParser();
        StreamProcessor processor = new StreamProcessor(is);
        XMLStreamReader reader = processor.getReader();
        while (reader.hasNext()) {
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equals("transaction")) {
                    TransactionEntity transaction = transactionParser.parse(reader);
                    Assert.assertEquals(entity, transactionDao.save(transaction));
                }
            }
            reader.next();
        }
        reader.close();
    }
}