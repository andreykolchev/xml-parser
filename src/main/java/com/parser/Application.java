package com.parser;

import com.parser.config.HibernateSessionFactory;
import com.parser.config.StreamProcessor;
import com.parser.dao.TransactionDao;
import com.parser.model.TransactionEntity;
import com.parser.parser.TransactionParser;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;


public class Application {


    public static void main(String[] args) throws IOException, XMLStreamException {
        Long counter = 0L;
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream("test.xml");
        if (is == null) throw new IOException("Failed to open file");
        TransactionParser transactionParser = new TransactionParser();
        TransactionDao transactionDao = new TransactionDao();
        StreamProcessor processor = new StreamProcessor(is);
        XMLStreamReader reader = processor.getReader();
        while (reader.hasNext()) {
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equals("transaction")) {
                    TransactionEntity transaction = transactionParser.parse(reader);
                    transactionDao.save(transaction);
                    System.out.println(++counter);
                }
            }
            reader.next();
        }
        reader.close();
        HibernateSessionFactory.shutdown();
    }
}
