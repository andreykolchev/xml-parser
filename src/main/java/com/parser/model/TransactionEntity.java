package com.parser.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "transaction")
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Float amount;

    private String place;

    private String currency;

    private String card;

    @Embedded
    private Client client;

}