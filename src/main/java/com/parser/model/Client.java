package com.parser.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Client {

    private String firstName;

    private String lastName;

    private String middleName;

    private Integer inn;
}
