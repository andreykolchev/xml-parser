package com.parser.parser;

import com.parser.model.Client;
import com.parser.model.TransactionEntity;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class TransactionParser {

    public TransactionEntity parse(XMLStreamReader reader) throws XMLStreamException {
        TransactionEntity transaction = new TransactionEntity();
        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT: {
                    String elementName = reader.getLocalName();
                    if ("place".equals(elementName)) transaction.setPlace(readCharacters(reader));
                    else if ("amount".equals(elementName)) transaction.setAmount(readFloat(reader));
                    else if ("currency".equals(elementName)) transaction.setCurrency(readCharacters(reader));
                    else if ("card".equals(elementName)) transaction.setCard(readCharacters(reader));
                    else if ("client".equals(elementName)) transaction.setClient(readClient(reader));
                    break;
                }
                case XMLStreamReader.END_ELEMENT: {
                    return transaction;
                }
            }
        }
        throw new XMLStreamException("End of file");
    }

    private Client readClient(XMLStreamReader reader) throws XMLStreamException {
        Client client = new Client();
        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT: {
                    String elementName = reader.getLocalName();
                    if ("firstName".equals(elementName)) client.setFirstName(readCharacters(reader));
                    else if ("lastName".equals(elementName)) client.setLastName(readCharacters(reader));
                    else if ("middleName".equals(elementName)) client.setMiddleName(readCharacters(reader));
                    else if ("inn".equals(elementName)) client.setInn(readInteger(reader));
                    break;
                }
                case XMLStreamReader.END_ELEMENT: {
                    return client;
                }
            }
        }
        throw new XMLStreamException("End of file");
    }


    private Integer readInteger(XMLStreamReader reader) throws XMLStreamException {
        String characters = readCharacters(reader);
        try {
            return Integer.valueOf(characters);
        } catch (NumberFormatException e) {
            throw new XMLStreamException("Invalid integer " + characters);
        }
    }

    private Float readFloat(XMLStreamReader reader) throws XMLStreamException {
        String characters = readCharacters(reader);
        try {
            return Float.valueOf(characters);
        } catch (NumberFormatException e) {
            throw new XMLStreamException("Invalid float " + characters);
        }
    }

    private String readCharacters(XMLStreamReader reader) throws XMLStreamException {
        StringBuilder result = new StringBuilder();
        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.CHARACTERS:
                case XMLStreamReader.CDATA:
                    result.append(reader.getText());
                    break;
                case XMLStreamReader.END_ELEMENT:
                    return result.toString();
            }
        }
        throw new XMLStreamException("End of file");
    }
}