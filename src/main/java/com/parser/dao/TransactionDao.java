package com.parser.dao;

import com.parser.config.HibernateSessionFactory;
import com.parser.model.TransactionEntity;
import org.hibernate.Session;

public class TransactionDao {

    public TransactionEntity save(TransactionEntity entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Long id = (Long) session.save(entity);
        session.getTransaction().commit();
        session.close();
        entity.setId(id);
        return entity;
    }
}
